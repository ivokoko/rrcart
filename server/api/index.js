import { Router } from 'express'
const router = new Router()

import db from '../db.js'


router.get('/products', (req, res) => {
  res.status(200).json(db)
})

router.get('/cart', (req, res) => {

  let cart = req.session.cart;
   console.log("CART")
  console.log(cart)
  if (cart && cart.length > 0) {


    res.status(200).send(cart)
  } else {
    res.status(200).send([])
  }
})

const productExists = function (cart, id) {
  return cart.map(function (obj) { return obj.id }).indexOf(id)
}

const calculatePrice = function(obj) {
  return obj.promo !== null?  (obj.price * obj.qty / obj.promo.amount * obj.promo.price).toFixed(2) : (obj.price * obj.qty).toFixed(2);
}

router.delete('/cart', (req, res) => {

  let cart = req.session.cart

  let index = productExists(cart, req.body.id)
  console.log(index)
  cart.splice(index,1)
  res.status(200).send(cart)
})

router.post('/cart', (req, res) => {

  let cart = req.session.cart
  if (!cart) {
    cart = req.session.cart = []
  }
  if (req.session.cart && req.session.cart.length > 0) {

    let index = productExists(cart, req.body.id)
    if (index > -1) {
      cart[index].qty += parseInt(req.body.qty)
      cart[index].totalPrice = calculatePrice(cart[index])
    } else {
      let item = req.body
      item.totalPrice = calculatePrice(item)
      cart.push(item)
    }
    res.status(200).send(cart)
  } else {
    let item = req.body
    item.totalPrice = calculatePrice(item)
    cart.push(item)
    res.status(200).send(cart)
  }
})

module.exports = router
