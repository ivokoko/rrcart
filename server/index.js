import http from 'http'
import express from 'express'
import helmet from 'helmet'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import morgan from 'morgan'
import compression from 'compression'
import hpp from 'hpp'
import throng from 'throng'
import React from 'react'
import ReactDOM from 'react-dom/server'
import { createMemoryHistory, RouterContext, match } from 'react-router'
import createRoutes from '../common/routes/root'
import { Provider } from 'react-redux'
import { trigger } from 'redial'
import { StyleSheetServer } from 'aphrodite'
import Helm from 'react-helmet'
import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import DefaultServerConfig from './config'
import webpackConfig from '../tools/webpack.client.dev'
import { compileDev, startDev } from '../tools/dx'
import { configureStore } from '../common/store'
const DashboardPlugin = require('webpack-dashboard/plugin')
var session = require('express-session')

process.noDeprecation = true

export const createServer = (config) => {

  const __PROD__ = config.nodeEnv === 'production'

  const app = express()
  let assets = null
  app.disable('x-powered-by')
  app.use(cookieParser());
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({extended: true}))



  if (__PROD__) {
    app.use(morgan('combined'))
    app.use(helmet())
    app.use(hpp())
    app.use(compression())
    assets = require('../assets.json')
  } else {
    app.use(morgan('dev'))
    const compiler = compileDev((webpack(webpackConfig)), config.port)
    // compiler.apply(new DashboardPlugin())
    app.use(webpackDevMiddleware(compiler, {
      quiet: true,
      watchOptions: {
        ignored: /node_modules/
      }
    }))
    app.use(webpackHotMiddleware(compiler, {log: console.log}))
  }

  app.use(express.static('public'))

  app.use(session({ secret: 'shhhhh', resave: false, httpOnly: false,
    saveUninitialized: true, cookie: { maxAge: 30*24*60*60000 }}))

  app.use('/api/v0/', require('./api'))

  app.get('*', (req, res) => {
    const store = configureStore({
      sourceRequest: {
        protocol: req.headers['x-forwarded-proto'] || req.protocol,
        host: req.headers.host,
        req:req.headers.cookie
      }
    })


    const routes = createRoutes(store)
    const history = createMemoryHistory(req.originalUrl)
    const {dispatch} = store

    match({routes, history}, (err, redirectLocation, renderProps) => {
      if (err) {
        console.error(err)
        return res.status(500).send('Internal server error')
      }
      if (!renderProps) {
        return res.status(404).send('Resource Not Found')
      }

      const {components} = renderProps


     // console.log(renderProps)

      const locals = {
        path: renderProps.location.pathname,
        query: renderProps.location.query,
        params: renderProps.params,
        req: req,
        dispatch
      }

      //console.log(JSON.stringify(renderProps))

      trigger('fetch', components, locals)
        .then(() => {
          const initialState = store.getState()
          const InitialView = (
            <Provider store={store}>
              <RouterContext {...renderProps} />
            </Provider>
          )
          const data = StyleSheetServer.renderStatic(
            () => ReactDOM.renderToString(InitialView)
          )
          const head = Helm.rewind()
          res.status(200).send(`
            <!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charSet="utf-8">
                <meta httpEquiv="X-UA-Compatible" content="IE=edge">
                ${head.title.toString()}
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="shortcut icon" href="/favicon.ico">
                ${head.meta.toString()}
                ${head.link.toString()}
                <style>
                  body {
                    margin: 0;
                    padding: 0;
                    font-size: 1rem;
                    background-color: #fff;
                    color: #555;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    font-family: "Source Sans Pro","Segoe UI","Segoe UI Light",HelveticaNeue-Light,"Roboto Thin",sans-serif;
                  }

                  h1,h2,h3,h4,h5,h6 {
                    margin: 0;
                    padding: 0;
                  }
                  
                </style>
                <style data-aphrodite>${data.css.content}</style>
              </head>
              <body>
                <div id="root"><div>${data.html}</div></div>
                <script>window.renderedClassNames = ${JSON.stringify(data.css.renderedClassNames)};</script>
                <script>window.INITIAL_STATE = ${JSON.stringify(initialState)};</script>
                <script src="${ __PROD__ ? assets.vendor.js : '/vendor.js' }"></script>
                <script async src="${ __PROD__ ? assets.main.js : '/main.js' }" ></script>
              </body>
            </html>
          `)

        }).catch(e => console.log(e))

    })
  })
  const server = http.createServer(app)

  if (config.timeout) {
    server.setTimeout(config.timeout, (socket) => {
      const message = `Timeout of ${config.timeout}ms exceeded`

      socket.end([
        'HTTP/1.1 503 Service Unavailable',
        `Date: ${(new Date).toGMTString()}`,  // eslint-disable-line
        'Content-Type: text/plain',
        `Content-Length: ${message.length}`,
        'Connection: close',
        '',
        message
      ].join(`\r\n`))
    })
  }

  return server
}
export const startServer = (serverConfig) => {
  const config =  {...DefaultServerConfig, ...serverConfig}
  const server = createServer(config)
  server.listen(config.port, (err) => {
    if (config.nodeEnv === 'production' || config.nodeEnv === 'test') {
      if (err) console.log(err)
      console.log(`server ${config.id} listening on port ${config.port}`)
    } else {
      startDev(config.port, err)
    }
  })
}

if (require.main === module) {
  throng({
    start: (id) => startServer({ id }),
    workers: process.env.WEB_CONCURRENCY || 1,
    lifetime: Infinity
  })
}
