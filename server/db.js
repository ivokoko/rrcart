const db = [
  {
    id: 1,
    name: 'Apple',
    price: 0.25,
    pack: 1,
    promo: null
  },
  {
    id: 2,
    name: 'Oranges',
    price: 0.30,
    pack: 1,
    promo: null
  },
  {
    id: 3,
    name: 'Banana',
    price: 0.15,
    pack: 1,
    promo: null
  },
  {
    id: 4,
    name: 'Papaya',
    price: 0.50,
    pack: 3,
    promo: {amount: 3, price: 2}
  }
]

export default db
