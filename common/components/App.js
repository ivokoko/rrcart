import React from 'react'
import Helmet from 'react-helmet'

const App = ({ children }) => (
  <div>
    <Helmet title='Cart' titleTemplate='%s - MyWebsite' />
    {children}
  </div>
)

export default App
