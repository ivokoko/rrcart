import * as types from '../../constants'

const initialState = {
  data: [],
  isLoading: false,
  error: null
}

export default function cart (state = initialState, action) {
  switch (action.type) {
    case types.ADD_TO_CART_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null
      }
    case types.ADD_TO_CART_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.payload
      }
    case types.ADD_TO_CART_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      }
    case types.LOAD_CART_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.payload
      }
    case types.REMOVE_FROM_CART_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.payload
      }
    default:
      return state
  }
}
export const getCart = state => state.cart
