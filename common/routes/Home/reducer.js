import * as types from '../../constants'

const initialState = {
  data: [],
  isLoading: false,
  error: null
}

export default function products (state = initialState, action) {
  switch (action.type) {
    case types.LOAD_PRODUCTS_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null
      }
    case types.LOAD_PRODUCTS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      }
    case types.LOAD_PRODUCTS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      }
    default:
      return state
  }
}
export const getProducts = state => state.products
