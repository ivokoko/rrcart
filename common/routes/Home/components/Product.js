import React from 'react'
import { StyleSheet, css } from 'aphrodite'
import { Link } from 'react-router'

const Product = React.createClass({

  getInitialState () {
    return {
      qty: this.props.product.pack
    }
  },
  setQty (e) {
    console.log(e.target.value)
    if (e.target.value % this.state.qty > 0) {
      window.alert(this.props.product.name + ' is available in packs of ' + this.props.product.pack + '. Like 3, 6, 9, etc... The minimum is ' + this.props.product.pack)
    } else {
      this.setState({qty: e.target.value})
    }
  },
  render () {
    return (
      <div style={{width: 50 + '%', float: 'left'}}>
        <h4 style={{marginBottom: 10}}>{this.props.product.name}</h4>
        <div style={{float: 'left', width: 100 + '%', marginBottom: 10}}>Price: {this.props.product.price.toFixed(2)}</div>
        <div><input type="text" style={{width: 20 + 'px', float: 'left'}} name="qty" onChange={this.setQty} value={this.state.qty}/></div>
        <div style={{float: 'left', marginLeft: 10, marginBottom: 20}}><Link onClick={() => this.props.cartadd(this.props.product, this.state.qty)}>Add to cart</Link></div>
      </div>
    )
  }
})
export default Product
