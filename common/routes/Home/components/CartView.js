import React from 'react'
import { Link } from 'react-router'

const CartView = React.createClass({

  calculatePrice (p) {
    let cartTotal = 0
    p.forEach(function (obj) {
      cartTotal += parseFloat(obj.totalPrice)
    })
    return parseFloat(cartTotal).toFixed(2) //   parseFloat(cartTotal).toFixed(2)
  },

  render () {
    return (
      <div>
        <h3>Cart</h3>
        <div className="cart">
          <table style={{width: 100 + '%'}}>
            <thead>
              <tr><td>Name</td><td style={{textAlign: 'right'}}>Qty</td><td style={{textAlign: 'right'}}>Price</td><td style={{textAlign: 'right'}}>Total</td><td></td></tr>
            </thead>
            <tbody>
            { this.props.products.length > 0
              ? this.props.products.map((r, i) => {
                return (
                  <tr>
                    <td>{r.name}</td>
                    <td style={{textAlign: 'right'}}>{r.qty}</td>
                    <td style={{textAlign: 'right'}}>{r.price.toFixed(2)}</td>
                    <td style={{textAlign: 'right'}}>{parseFloat(r.totalPrice).toFixed(2)}</td>
                    <td><Link onClick={() => this.props.remove(r)}>X</Link></td>
                  </tr>
                )
              })
              : <tr>
                <td colSpan={4}>Cart is Empty</td>
              </tr>
            }
            <tr>
              <td colSpan={3} style={{paddingTop: 10, textAlign: 'right'}}>Order Total:</td>
              <td style={{paddingTop: 10, textAlign: 'right'}}>{this.calculatePrice(this.props.products)}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
})

export default CartView
