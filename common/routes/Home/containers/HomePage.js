import { provideHooks } from 'redial'
import React, { PropTypes } from 'react'
import { loadProducts, loadCart, addToCart, removeFromCart } from '../actions'
import { connect } from 'react-redux'
import { getProducts } from '../reducer'
import { getCart } from '../cartReducer'
import Product from '../components/Product'
import CartView from '../components/CartView'

const redial = {
  fetch: ({dispatch}) => {
    return dispatch(loadProducts()).then(() => dispatch(loadCart()))
  }
}
const mapStateToProps = state => ({
  products: getProducts(state),
  cart: getCart(state)
})

const HomePage = ({products, cart, dispatch}) => {
  const AddToCart = (p, q) => {
    p.qty = q
    console.log(JSON.stringify(p))
    dispatch(addToCart(p))
  }

  const remove = (p) => {
    dispatch(removeFromCart(p))
  }

  return (
    <div style={{width: 85 + '%', margin: 'auto', marginTop: 50}}>
      <h3 style={{width: 100 + '%', marginTop: 10, marginBottom: 10}}>Products</h3>
      <div style={ { minWidth: 75 + '%', float: 'left' } }>
        {products.isLoading &&
        <div>Loading...</div>
        }
        {
          !products.isLoading &&
          products.data.map((r, i) => (
            <Product cartadd={AddToCart} product={r}/>
          ))
        }
      </div>
      <div style={{width: 25 + '%', float: 'left'}}>
        {!cart.isLoading &&
          <CartView remove={remove} products={cart.data}/>
        }
      </div>
    </div>
  )
}

HomePage.PropTypes = {
  products: PropTypes.array.isRequired
}

export default provideHooks(redial)(connect(mapStateToProps)(HomePage))
