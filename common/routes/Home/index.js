if (typeof require.ensure !== 'function') require.ensure = (d, c) => c(require)
import { injectAsyncReducer } from '../../store'

export default function createRoutes (store) {
  return {
    getComponents (location, callback) {
      require.ensure([
        './containers/HomePage',
        './reducer',
        './cartReducer'
      ], (require) => {
        let HomePage = require('./containers/HomePage').default
        let homeReducer = require('./reducer').default
        let cartReducer = require('./cartReducer').default
        injectAsyncReducer(store, 'products', homeReducer)
        injectAsyncReducer(store, 'cart', cartReducer)
        callback(null, HomePage)
      })
    }
  }
}
