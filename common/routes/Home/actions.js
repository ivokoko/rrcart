import {LOAD_PRODUCTS_REQUEST, LOAD_PRODUCTS_FAILURE, LOAD_PRODUCTS_SUCCESS,
        ADD_TO_CART_REQUEST, ADD_TO_CART_FAILURE, ADD_TO_CART_SUCCESS,
        REMOVE_FROM_CART_REQUEST, REMOVE_FROM_CART_FAILURE, REMOVE_FROM_CART_SUCCESS,
        LOAD_CART_REQUEST, LOAD_CART_FAILURE, LOAD_CART_SUCCESS
} from '../../constants'

export function loadProducts () {
  return (dispatch, getState, {axios}) => {
    const { protocol, host } = getState().sourceRequest
    dispatch({ type: LOAD_PRODUCTS_REQUEST })
    return axios.get(`${protocol}://${host}/api/v0/products`)
      .then(res => {
        dispatch({
          type: LOAD_PRODUCTS_SUCCESS,
          payload: res.data
        })
      })
      .catch(error => {
        dispatch({
          type: LOAD_PRODUCTS_FAILURE,
          payload: error,
          error: true
        })
      })
  }
}

export function loadCart () {
  return (dispatch, getState, {axios}) => {
    const {protocol, host, req} = getState().sourceRequest
    dispatch({ type: LOAD_CART_REQUEST })
    return axios({method: 'GET', url: `${protocol}://${host}/api/v0/cart`, headers: {'Cookie': req}})
      .then(res => {
        dispatch({
          type: LOAD_CART_SUCCESS,
          payload: res.data
        })
      })
      .catch(error => {
        dispatch({
          type: LOAD_CART_FAILURE,
          payload: error,
          error: true
        })
      })
  }
}

export function removeFromCart (product) {
  return (dispatch, getState, {axios}) => {
    const { protocol, host, req } = getState().sourceRequest
    dispatch({type: REMOVE_FROM_CART_REQUEST})

    let options = {
      method: 'DELETE',
      url: `${protocol}://${host}/api/v0/cart`,
      data: product
    }
    return axios(options)
      .then(res => {
        dispatch({
          type: REMOVE_FROM_CART_SUCCESS,
          payload: res.data
        })
      })
      .catch(error => {
        dispatch({
          type: REMOVE_FROM_CART_FAILURE,
          payload: error,
          error: true
        })
      })
  }
}

export function addToCart (product) {
  return (dispatch, getState, {axios}) => {
    const { protocol, host } = getState().sourceRequest
    dispatch({type: ADD_TO_CART_REQUEST})

    let options = {
      method: 'POST',
      url: `${protocol}://${host}/api/v0/cart`,
      data: product
    }

    return axios(options)
      .then(res => {
        dispatch({
          type: ADD_TO_CART_SUCCESS,
          payload: res.data
        })
      })
      .catch(error => {
        dispatch({
          type: ADD_TO_CART_FAILURE,
          payload: error,
          error: true
        })
      })
  }
}
